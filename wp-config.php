<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'codeu_movies' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':pay14Xow_S7aTV:}Ebql}vdA.%ZTTZ*,*C;9%|fQ>Bua}KXf5%FW*aIOHSE|[FA' );
define( 'SECURE_AUTH_KEY',  'p?~0Ue_fj[(PI;T{W[CD:3T?lkzc?/K/Gnn|JJCd:b`23;7E=!sU!`||!w3:F_vG' );
define( 'LOGGED_IN_KEY',    'w}HkB+}8mT8QlksPymke1!PT=h=nG_dUqfgYRi!Y~XE[{&M-6N()b39%tS4q:2zb' );
define( 'NONCE_KEY',        'v,K(_Os`Cz>|D&FGoRbhwJl}>)q,t<]o/XqUzyI8R!KZf0B+5{E2Qr-g!{X&O5?n' );
define( 'AUTH_SALT',        '6$Q< 1&HM7a L ;KzaVxR ERL6GO?DrFK3Eky[LG1=WBJ{l)E3p5|`{)KbwmiM*R' );
define( 'SECURE_AUTH_SALT', 'DPgiqg;*px2[H:?&LI(MfxRmf6em&GT+c~4Z=fQRd)p%uyf`e5B$:#j}2{Wlcz)!' );
define( 'LOGGED_IN_SALT',   '*@]VU[P7O[19NOtP!i #bMUf@XEl`H+9y-kz!(t.;RFaXBpTf?{[&k T(5tl9~^{' );
define( 'NONCE_SALT',       'Wfe ej5FwNS:r~lqB76N=YVFKpsaP]A=U?6~;%W/)aoZQv:}OSVM%V[4o:Jzc.&q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
