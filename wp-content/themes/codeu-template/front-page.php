<?php

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package CodeU_Template
 */

get_header();
?>

<?php
$heroTitle = get_field('hero_title');
$heroButtonText = get_field('hero_button_text');
$heroButtonLink = get_field('hero_button_link');
$heroBanner = get_field('hero_banner');
?>


<main role="main" class="flex-shrink-0">
	<section id="hero" style="background-image: url('<?php echo $heroBanner['url']; ?>');">
		<div class="container">
			<div class="row">
				<div class="col">
					<h1><?php echo $heroTitle; ?></h1>

					<a href="<?php echo $heroButtonLink; ?>" class="button secondary-button">
						<?php echo $heroButtonText; ?>
					</a>
				</div>
			</div>
		</div>
	</section>
</main>

<section role="new-releases" class="movie-row">
	<div class="container">
		<h2 class="section-title">New Releases</h2>

		<!-- Slider main container -->
		<div class="swiper-container">
			<!-- Additional required wrapper -->
			<div class="swiper-wrapper">
				<?php
				$args = array(
					'post_type' => 'movies',
					'post_status' => 'publish',
					'posts_per_page' => 6,
					'orderby' => "title",
					'order' => "ASC",
				);

				$loop = new WP_Query($args);

				while ($loop->have_posts()) : $loop->the_post(); ?>

					<div class="movie-item swiper-slide">

						<div class="movie-poster">
							<?php the_post_thumbnail(); ?>
						</div>

						<h4 class="movie-title">
							<?= the_title(); ?>
						</h4>

						<div class="movie-rating">
							<p>
								<?php echo get_field('rating_percent'); ?>% on <a href="<?php the_field('rating_link'); ?>" target="_blank">Rotten Tomatoes</a>
							</p>
						</div>

						<div class="movie-tags">
							tags
						</div>

						<div class="movie-link">
							<a href="<?php echo get_field('rating_link'); ?>" target="_blank">Get Tickets</a>
						</div>

					</div>

				<?php endwhile;

				wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
