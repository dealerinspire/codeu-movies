/**
 * File codeu-custom.js.
 *
 * This file is used for customized JS/jQuery on our CodeU site.
 *
 */

jQuery(document).ready(function($){

	var mySwiper = new Swiper('.swiper-container', {
		speed: 400,
		slidesPerView: 3,
		spaceBetween: 100,
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		}
	});

});

document.addEventListener('DOMContentLoaded', function (event) {
	var tabNames = document.querySelectorAll('.tab-names > *')
	var tabContents = document.querySelectorAll('.tab-contents > *')

	tabNames.forEach(function (tabName) {
		tabName.addEventListener('click', function () {

			// Loop through tab names, remove .active
			tabNames.forEach(function (name) {
				name.classList.remove('active')
			})

			// Loop through tab contents, remove .active
			tabContents.forEach(function (content) {
				content.classList.remove('active')
			})

			// Add active class to the name you clicked.
			tabName.classList.add('active')

			// Add active class to the tab you clicked.
			var index = getIndex(tabName) // 0, 1, or 2
			tabContents[index].classList.add('active')

		})
	})

	function getIndex(node) {
		var index = 0;
		while ((node = node.previousElementSibling)) {
			index++;
		}
		return index;
	}

});